## Upload image sample

This Sample demonstrates how to upload a ripple
to be shown in Ripple Makers queues.


### Usage from cli

Uploading a Ripple to a location queue is pretty stright forward:

`node app.js --location "location-id" --token "token-string" --image "absolute/path/to/image"`

>`--image` paramter is optional, if missing, a default `sample.png` Ripple will be sent. 

For more information: `node app.js -h` or view source code.