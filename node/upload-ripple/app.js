const program = require('commander');
const fetch = require('node-fetch');
const package = require('./package.json');
const fs = require('fs');

const DEFAULT_IMAGE_PATH = `${__dirname}/sample.jpg`;

program
    .version(package.version, '-v, --version')
    .usage('node app.js --image path-to-image --location location-id --token token-string')
    .option('-i, --image [image]', 'path of image to upload', DEFAULT_IMAGE_PATH)
    .option('-l, --location <location>', 'id of location to upload to image to')
    .option('-t, --token <token>', 'api token for the uploading')
    .allowUnknownOption(false)
    .parse(process.argv);

const { image, location, token } = program;

if (!location) {
    console.log('location id is required! pass it with -l or --location');
    process.exit(1);
}

if (!token) {
    console.log('token is required! pass it with -t or --token');
    process.exit(1);
}

const url = `https://mobile.coffeeripples.com/api/client/v1/locations/${location}/image`;

fetch(url, {
    method: 'POST',
    body: fs.readFileSync(image),
    headers: {
        'Content-Type': 'image/png',
        'Authorization': token
    }
})
.then(res => res.json())
.then(json => console.log(json));

